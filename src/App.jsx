import React, { useState, useEffect } from "react";

import sendRequest from "./helpers/sendRequest.js";

import Header from "./components/Header";
import ModalAddProduct from "./components/Modal";
import SectionPruducts from "./components/Section";

const App = () => {
  const [isModalProduct, setIsModalProduct] = useState(false);
  const [products, setProducts] = useState([]);
  const [currentProduct, setCurrentProduct] = useState({});
  const [carts, setToCart] = useState(
    JSON.parse(localStorage.getItem("carts")) || []
  );
  const [favorites, setFavorite] = useState(
    JSON.parse(localStorage.getItem("favorites")) || []
  );

  useEffect(() => {
    const savedProducts = JSON.parse(localStorage.getItem("products"));
    if (savedProducts) {
      setProducts(savedProducts);
    } else {
      sendRequest("products.json").then((data) => {
        setProducts(
          data.map((product) => ({
            ...product,
            addedToCart: false,
            addedToFavorite: false,
          }))
        );
      });
    }
  }, []);

  const handleToCart = (item) => {
    setProducts((prevState) => {
      const updatedProducts = prevState.map((product) =>
        product.vendorCode === item.vendorCode
          ? { ...product, addedToCart: true }
          : product
      );
      localStorage.setItem("products", JSON.stringify(updatedProducts));
      return updatedProducts;
    });

    setToCart((prevState) => {
      const newState = [...prevState, item];
      localStorage.setItem("carts", JSON.stringify(newState));
      return newState;
    });
  };

  const handleFavorite = (item) => {
    if (item.addedToFavorite) {
      const updatedFavorites = favorites.filter(
        (fav) => fav.vendorCode !== item.vendorCode
      );
      setFavorite(updatedFavorites);

      setProducts((prevState) => {
        const updatedProducts = prevState.map((product) =>
          product.vendorCode === item.vendorCode
            ? { ...product, addedToFavorite: false }
            : product
        );
        localStorage.setItem("products", JSON.stringify(updatedProducts));
        return updatedProducts;
      });

      localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
    } else {
      setFavorite((prevState) => [...prevState, item]);
      setProducts((prevState) => {
        const updatedProducts = prevState.map((product) =>
          product.vendorCode === item.vendorCode
            ? { ...product, addedToFavorite: true }
            : product
        );
        localStorage.setItem("products", JSON.stringify(updatedProducts));
        return updatedProducts;
      });
      localStorage.setItem("favorites", JSON.stringify([...favorites, item]));
    }
  };

  const handleModalProduct = () => {
    setIsModalProduct((prevState) => !prevState);
  };

  const handlCurrentProduct = (product) => {
    setCurrentProduct(product);
  };

  return (
    <div className="container">
      <Header countCarts={carts.length} countFavorites={favorites.length} />

      <SectionPruducts
        handleModalProduct={handleModalProduct}
        onCurrentProduct={handlCurrentProduct}
        products={products}
        handleFavorite={handleFavorite}
      />

      <ModalAddProduct
        isOpen={isModalProduct}
        onClose={handleModalProduct}
        currentProduct={currentProduct}
        handleModalProduct={handleModalProduct}
        onAddToCart={() => {
          handleToCart(currentProduct);
        }}
      />
    </div>
  );
};

export default App;
