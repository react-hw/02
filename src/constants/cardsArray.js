const cardsArray = [
  {
    vendorCode: "0001",
    name: "Optimus Prime",
    price: "$40",
    path: "./Images/01_optimus-prime.png",
    color: "red",
  },
  {
    vendorCode: "0002",
    name: "Bumblebee",
    price: "$40",
    path: "./public/Images/01_optimus-prime.png",
    color: "yellow",
  },
  {
    vendorCode: "0003",
    name: "Optimus Black Convoy",
    price: "$40",
    path: "../../public/Images/01_optimus-prime.png",
    color: "black",
  },
  {
    vendorCode: "0004",
    name: "Megatron",
    price: "$40",
    path: "../../public/Images/01_optimus-prime.png",
    color: "silver",
  },
  {
    vendorCode: "0005",
    name: "Giocattoli Police",
    price: "$40",
    path: "../../public/Images/01_optimus-prime.png",
    color: "red",
  },
  {
    vendorCode: "0006",
    name: "Rhinox",
    price: "$39,54",
    path: "../../public/Images/01_optimus-prime.png",
    color: "red",
  },
  {
    vendorCode: "0007",
    name: "Ultra Magnus",
    price: "$40",
    path: "../../public/Images/01_optimus-prime.png",
    color: "red",
  },
  {
    vendorCode: "0008",
    name: "Baraka",
    price: "$40",
    path: "../../public/Images/01_optimus-prime.png",
    color: "red",
  },
  {
    vendorCode: "0009",
    name: "Sonya Blade",
    price: "$40",
    path: "../../public/Images/01_optimus-prime.png",
    color: "red",
  },
  {
    vendorCode: "00010",
    name: "Batman",
    price: "$40",
    path: "../../public/Images/01_optimus-prime.png",
    color: "black",
  },
  {
    vendorCode: "00011",
    name: "Captain America",
    price: "$40",
    path: "../../public/Images/01_optimus-prime.png",
    color: "blue",
  },
  {
    vendorCode: "00012",
    name: "Hulk",
    price: "$40",
    path: "../../public/Images/01_optimus-prime.png",
    color: "green",
  },
];

const cardsJSON = JSON.stringify(cardsArray, null, 2);
export default cardsJSON;

// console.log(cardsJSON);

// import { sendRequest } from "../helpers/sendRequest";
// sendRequest (cardsJSON)

// async function sendRequest (url) {
//   const response = await fetch(url);
//   const result = await response.json();
//   console.log(result);
//   return result;
// }

// sendRequest (cardsJSON);