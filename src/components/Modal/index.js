import ModalAddProduct from "./ModalAddProduct";
import ModalImage from "./ModalImage";
import ModalText from "./ModalText";

export default ModalAddProduct;
export { ModalImage, ModalText };
