import React from "react";
import PropTypes from "prop-types";

import ModalWrapper, {
  ModalBox,
  ModalContainer,
  ModalHeader,
  ModalMain,
  ModalFooter,
} from "./components";

import ButtonCornerClose from "../Button/ButtonCornerClose";
import Button from "../Button";

const ModalAddProduct = (props) => {
  const { isOpen, onClose, currentProduct, handleModalProduct, onAddToCart } =
    props;

  const { name, path } = currentProduct;

  return (
    <ModalWrapper isOpen={isOpen} onClick={onClose}>
      <ModalBox>
        <ButtonCornerClose onClick={handleModalProduct} />
        <ModalContainer>
          <ModalHeader>
            <div className="modal__header-img-box">
              <img src={path} alt={name} className="modal__header-img" />
            </div>
          </ModalHeader>

          <ModalMain currentProduct={currentProduct} />

          <ModalFooter>
            <Button
              onClick={() => {
                handleModalProduct();
                onAddToCart();
              }}
            >
              ADD TO CART
            </Button>
          </ModalFooter>
        </ModalContainer>
      </ModalBox>
    </ModalWrapper>
  );
};

ModalAddProduct.propTypes = {
  isOpen: PropTypes.bool,
  currentProduct: PropTypes.object,
  onClose: PropTypes.func,
  handleModalProduct: PropTypes.func,
  onAddToCart: PropTypes.func,
};

export default ModalAddProduct;
