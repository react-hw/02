import React from "react";
import PropTypes from "prop-types";

import "./ModalBase.scss";

const ModalMain = ({ bodyTitle, bodyText, currentProduct }) => {
  const { name, price, color, vendorCode } = currentProduct;
  return (
    <div className="modal__main">
      <p className="modal__main-title">
        Add <span>{name}</span> to cart
      </p>
      <div className="modal__main-text">
        <p>Price: ${price}</p>
        <p>Color: {color}</p>
        <p>Article: {vendorCode}</p>
      </div>
      {/* {bodyTitle && (
        <p className="modal__main-title">
          Add{" "}
          <b>
            <span>{bodyTitle}</span>
          </b>{" "}
          to cart
        </p>
      )} */}
      {/* {bodyText && <p className="modal__main-text">{bodyText}</p>} */}
    </div>
  );
};

ModalMain.propTypes = {
  bodyTitle: PropTypes.string,
  bodyText: PropTypes.string,
  currentProduct: PropTypes.object,
};

export default ModalMain;
