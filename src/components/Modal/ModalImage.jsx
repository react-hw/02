import React from "react";
import PropTypes from "prop-types";

import "./ModalImage.scss";

import ModalWrapper, {
  ModalBox,
  ModalContainer,
  ModalHeader,
  ModalMain,
  ModalFooter,
} from "./components";

import ButtonCornerClose from "../Button/ButtonCornerClose";

const ModalImage = (props) => {
  const { isOpen, onClose, children } = props;

  return (
    <ModalWrapper isOpen={isOpen} onClick={onClose}>
      <ModalBox>
        <ButtonCornerClose onClick={onClose} />
        <ModalContainer>
          <ModalHeader>
            <div className="modal-image__header-wrapper">
              <img
                className="modal-image__header-img"
                scr=""
                alt="Blank Image Template"
              />
            </div>
          </ModalHeader>
          <ModalMain
            bodyTitle="Product Delete!"
            bodyText="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."
          />
          <ModalFooter>{children}</ModalFooter>
        </ModalContainer>
      </ModalBox>
    </ModalWrapper>
  );
};

ModalImage.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  children: PropTypes.any,
};

export default ModalImage;
