import React from "react";
import PropTypes from "prop-types";

import ModalWrapper, {
  ModalBox,
  ModalContainer,
  ModalMain,
  ModalFooter,
} from "./components";

import ButtonCornerClose from "../Button/ButtonCornerClose";

const ModalText = (props) => {
  const { isOpen, onClose, children } = props;
  return (
    <ModalWrapper isOpen={isOpen} onClick={onClose}>
      <ModalBox>
        <ButtonCornerClose onClick={onClose} />
        <ModalContainer>
          <ModalMain
            bodyTitle='Add Product "NAME"'
            bodyText="Description for you product"
          />
          <ModalFooter>{children}</ModalFooter>
        </ModalContainer>
      </ModalBox>
    </ModalWrapper>
  );
};

ModalText.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  children: PropTypes.any,
};

export default ModalText;
