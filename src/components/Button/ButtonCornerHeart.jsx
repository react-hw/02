import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import { ButtonCorner } from "./";
import { Heart } from "../../assets";

const ButtonCornerHeart = ({ onClick, isActive }) => {
  return (
    <ButtonCorner onClick={onClick}>
      <Heart className={cn("svg--heart", { active: isActive })} />
    </ButtonCorner>
  );
};

ButtonCornerHeart.propTypes = {
  onClick: PropTypes.func,
  isActive: PropTypes.bool,
};

export default ButtonCornerHeart;
