import Button from "./Button";
import ButtonCorner from "./ButtonCorner";
import ButtonCornerClose from "./ButtonCornerClose";
import ButtonCornerHeart from "./ButtonCornerHeart";
import ButtonCornerCart from "./ButtonCornerCart";
import ButtonPlate from "./ButtonPlate.jsx";

export default Button;
export {
  ButtonCorner,
  ButtonCornerClose,
  ButtonCornerHeart,
  ButtonCornerCart,
  ButtonPlate,
};
