import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import { ButtonCorner } from "./";
import { Cart } from "../../assets/index.js";

const ButtonCornerCart = ({ onClick, isActive }) => {
  return (
    <ButtonCorner className="btn__corner-left" onClick={onClick}>
      <Cart className={cn("svg--cart", { active: isActive })} />
    </ButtonCorner>
  );
};

ButtonCornerCart.propTypes = {
  onClick: PropTypes.func,
  isActive: PropTypes.bool,
};

export default ButtonCornerCart;
