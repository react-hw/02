import React, { useState } from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Card.scss";

import { ButtonCornerCart, ButtonCornerHeart } from "../Button";

const Card = (props) => {
  const { className, product, onModalProduct, handleFavorite } = props;
  const { name, price, color, vendorCode, path } = product;

  return (
    <div className={cn("card", className)}>
      <ButtonCornerCart
        onClick={onModalProduct}
        isActive={product.addedToCart}
      />
      <ButtonCornerHeart
        onClick={() => {
          handleFavorite(product);
        }}
        isActive={product.addedToFavorite}
      />

      <div className={cn("card__content", className)}>
        <header className={cn("card__header", className)}>{name}</header>
        <main className={cn("card__main", className)}>
          <img src={path} alt={name} className="card__main-img" />
        </main>
        <footer className={cn("card__footer", className)}>
          <p>Price: ${price}</p>
          <p>Color: {color}</p>
          <p>Article: {vendorCode}</p>
        </footer>
      </div>
    </div>
  );
};

Card.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object,
  onModalProduct: PropTypes.func,
  handleFavorite: PropTypes.func,
};

export default Card;
