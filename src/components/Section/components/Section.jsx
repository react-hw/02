import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Section.scss";

const Section = ({ className, children }) => {
  return (
    <section className={cn("inner", "section", className)}>{children}</section>
  );
};

Section.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

export default Section;
