import React from "react";
import PropTypes from "prop-types";

import Section, { SectionWrap } from "./components";
import Card from "../Card";

const SectionPruducts = (props) => {
  const { products, handleModalProduct, onCurrentProduct, handleFavorite } =
    props;
  return (
    <Section>
      <SectionWrap>
        {products.map((product) => {
          return (
            <Card
              key={product.vendorCode}
              product={product}
              onModalProduct={() => {
                handleModalProduct();
                onCurrentProduct(product);
              }}
              handleFavorite={handleFavorite}
            />
          );
        })}
      </SectionWrap>
    </Section>
  );
};

SectionPruducts.propTypes = {
  products: PropTypes.array,
  handleModalProduct: PropTypes.func,
  onCurrentProduct: PropTypes.func,
  handleFavorite: PropTypes.func,
};

export default SectionPruducts;
