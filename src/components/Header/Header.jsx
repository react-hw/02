import React, { useState } from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import { Heart, Cart } from "../../assets";
import { ButtonPlate } from "../Button";

import "./Header.scss";

const Header = ({ className, countFavorites, countCarts }) => {
  const showCartCountZero = countCarts == 0;
  const showCartCountDecade = countCarts > 0 && countCarts < 100;

  const showFavoriteCountZero = countFavorites == 0;
  const showFavoriteCountDecade = countFavorites > 0 && countFavorites < 100;

  return (
    <header className={cn("inner", "header", className)}>
      <div className="logo">
        <a href="#" className="logo__link">
          <div className="logo__box">
            <p className="logo__text">logo</p>
          </div>
        </a>
        <p className="logo__name">Action Figures</p>
      </div>
      <div className="menu">
        <ButtonPlate className="btn__plate">
          <Cart
            className={cn(
              "svg__plate",
              { svg_cart: showCartCountZero },
              { svg_cart_full: showCartCountDecade }
            )}
          />
          {showCartCountDecade && (
            <span className="counter_plate">{countCarts}</span>
          )}
        </ButtonPlate>

        <ButtonPlate className="btn__plate">
          <Heart
            className={cn(
              "svg__plate",
              { svg_heart_empty: showFavoriteCountZero },
              { svg_heart_full: showFavoriteCountDecade }
            )}
          />
          {showFavoriteCountDecade > 0 && (
            <span className="counter_plate">{countFavorites}</span>
          )}
        </ButtonPlate>
      </div>
    </header>
  );
};

Header.propTypes = {
  className: PropTypes.string,
  countFavorites: PropTypes.number,
  countCarts: PropTypes.number,
};

export default Header;
